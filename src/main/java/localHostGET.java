import io.restassured.http.ContentType;
import org.json.simple.JSONObject;
import org.junit.Test;

import static io.restassured.RestAssured.*;

public class localHostGET {

    @Test
    public void GET(){

        baseURI= ("http://localhost:3000/");
        given().
                param("ID",1).
           get("/users").
        then().
           statusCode(200)
        .log().all();
    }


    @Test
    public void POST(){

        JSONObject request = new JSONObject();

        request.put("firstName","second_one");
        request.put("lastName","second");
        request.put("subjectId",10);

        baseURI=("http://localhost:3000/");

        given().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                headers("Content-Type","application/json").
                body(request.toJSONString()).
        when().
                post("/users").
        then().
                statusCode(201).
                log().all();
    }

    @Test
    public void PATCH(){

        JSONObject request = new JSONObject();

        request.put("firstName","changed");


        baseURI=("http://localhost:3000/");

        given().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                headers("Content-Type","application/json").
                body(request.toJSONString()).
        when().
                patch("/users/6").
        then().
                statusCode(200).
                log().all();
    }

    @Test
    public void PUT(){

        JSONObject request = new JSONObject();

        request.put("firstName","maro");
        request.put("lastName","mohamed");
        request.put("subjectId",9);

        baseURI=("http://localhost:3000/");

        given().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                headers("Content-Type","application/json").
                body(request.toJSONString()).
                when().
                put("/users/6").
                then().
                statusCode(200).
                log().all();
    }

    @Test
    public void DELETE(){

        baseURI=("http://localhost:3000/");

        when().
                delete("/users/4").
        then().
                statusCode(200).
                log().all();
    }
}
